﻿using System;
using System.Collections.Generic;
using ClassLibrary2;
using Microsoft.Extensions.Configuration;

namespace ConsoleApp1
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var builder = new ConfigurationBuilder();
			builder.AddCommandLine(args, new Dictionary<string, string>
			{
				["-Name"] = "Name"
			});
			var config = builder.Build();
			var name = config["Name"];
			Console.WriteLine(NameLibrary.GetName(name));
		}
	}
}