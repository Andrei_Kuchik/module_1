﻿using System;
using System.Windows.Forms;
using ClassLibrary2;

namespace WindowsFormsApp1
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			var name = textBox1.Text;
			MessageBox.Show(NameLibrary.GetName(name));
		}
	}
}