﻿using System;
using System.Text.RegularExpressions;

namespace ClassLibrary2
{
	public class NameLibrary
	{
		private static readonly Regex validation = new Regex("^[a-zA-Z0-9]*$");

		public static string GetName(string name)
		{
			if (validation.IsMatch(name))
			{
				return $"{DateTime.Now} Hello {name}!";
			}
			else
			{
				return "Error, type valid string!";
			}
		}
	}
}
