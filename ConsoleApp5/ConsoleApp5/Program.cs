﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ClassLibrary2;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.CommandLine;
namespace ConsoleApp5
{
    class Program
    {
	    static readonly Regex validation = new Regex("^[a-zA-Z0-9]*$");

		static void Main(string[] args)
        {
			var builder = new ConfigurationBuilder();
	        builder.AddCommandLine(args, new Dictionary<string, string>
	        {
		        ["-Name"] = "Name"
	        });
	        var config = builder.Build();
	        var name = config["Name"];
	        Console.WriteLine(NameLibrary.GetName(name));
	       
        }
    }
}
